﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using System.Xml;
using System.Runtime.Remoting;

namespace SqlTester
{
    static public class DbConnectionCollection
    {
        //static public IDbConnection GetProviderConnection(string in_pname)
        //{
        //    switch (in_pname)
        //    {
        //        case "Microsoft SQL Server":
        //            {
        //                return new SqlConnection();
        //            }
        //        case "MySQL Server":
        //            {
        //                return new MySqlConnection();
        //            }
        //        case "Oracle Server":
        //            {
        //                return new OracleConnection();
        //            }
        //        default:
        //            throw new Exception();
        //    }
        //}

        public static IDbConnection CreateConnection(string in_pname)
        {
            IDbConnection connection = null;
            var conn_namespace = GetConnectionNamespace(in_pname);
            var conn_type = GetConnectionClass(in_pname);

            var type = typeof(IDbConnection);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p));

            foreach (Type t in types)
            {
                if (t.Namespace == conn_namespace)
                {
                    var conn_inst = Activator.CreateInstance(t.Assembly.FullName, string.Format("{0}.{1}", conn_namespace, conn_type));
                    var conn_obj = ((ObjectHandle)conn_inst).Unwrap();
                    connection = conn_obj as IDbConnection;
                }
            }
            return connection;
        }

        private static string GetConnectionClass(string name)
        {
            string tname = null;
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load("ProvList.xml");
            for (int i = 0; i < xdoc.ChildNodes[1].ChildNodes.Count; i++)
            {
                var current_element = xdoc.ChildNodes[1].ChildNodes[i];
                if (current_element.Attributes["name"].Value == name)
                    tname = current_element.Attributes["type"].Value;
            }
            return tname;
        }

        private static string GetConnectionNamespace(string name)
        {
            string nsp = null;
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load("ProvList.xml");
            for (int i = 0; i < xdoc.ChildNodes[1].ChildNodes.Count; i++)
            {
                var current_element = xdoc.ChildNodes[1].ChildNodes[i];
                if (current_element.Attributes["name"].Value == name)
                    nsp = current_element.Attributes["nspace"].Value;
            }
            return nsp;
        }
    }

    public class DbExceptionCollection : DbException
    {
        public string Code { get; set; }

        public DbExceptionCollection()
        {
            //IDbConnection connection = null;
            //var conn_namespace = GetConnectionNamespace(in_pname);
            //var conn_type = GetConnectionClass(in_pname);

            NewMethod();

        }

        private void NewMethod()
        {
            //var ocon = new OracleCommand();
            var ass = AppDomain.CurrentDomain.GetAssemblies();
            var base_type = this.GetType().BaseType;
            var type = typeof(DbException);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p));
            
            foreach (Type t in types)
            {
                //if (t.Namespace == conn_namespace)
                //{
                //    var conn_inst = Activator.CreateInstance(t.Assembly.FullName, string.Format("{0}.{1}", conn_namespace, conn_type));
                //    var conn_obj = ((ObjectHandle)conn_inst).Unwrap();
                //    connection = conn_obj as IDbConnection;
                //}
            }
        }
    }
}
