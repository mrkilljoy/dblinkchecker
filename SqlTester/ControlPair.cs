﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SqlTester
{
    /// <summary>
    /// Контейнер для контролов, содержащих информацию
    /// о параметрах строки подключения выбранного провайдера.
    /// </summary>
    public class ControlPair : Panel
    {
        Label _desc;
        TextBox _txt;

        public ControlPair(string desc)
        {
            _desc = new Label();
            _desc.Text = desc;
            _txt = new TextBox();
            SetProportions();
            this.Controls.Add(_desc);
            this.Controls.Add(_txt);
        }

        void SetProportions()
        {
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            _desc.Width = 100;
            _desc.Location = new System.Drawing.Point(5, 5);
            _txt.Width = 80;
            _txt.Location = new System.Drawing.Point(_desc.Location.X + _desc.Width, 5);
        }

        public string TextContent
        {
            get { return _txt.Text; }
            set { _txt.Text = value; }
        }

        public string Description
        {
            get { return _desc.Text; }
            set { _desc.Text = value; }
        }
    }
}
