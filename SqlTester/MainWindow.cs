﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Oracle.ManagedDataAccess.Client;

namespace SqlTester
{
    /// <summary>
    /// Главная форма
    /// </summary>
    public partial class MainWindow : Form
    {
        // словарь, содержащий список провайдеров, а также набор параметров для строки подключения
        private Dictionary<string, Dictionary<string,string>> _provider_params;

        public MainWindow()
        {
            InitializeComponent();
            InitializeProviderList();
            ApplyHandlers();
            AutoLocalizer.ApplyLocalization(this, this.LocalizeFile);
        }

        // свойство, возвращающее или задающее имя файла
        // с текущей локализацией
        public string LocalizeFile
        {
            get 
            {
                string current = Properties.Settings.Default.loc_filename_current;
                if (current == null || current == "")
                    return Properties.Settings.Default.loc_filename_default;
                else
                    return current;
            }

            set
            {
                Properties.Settings.Default.loc_filename_current = value;
            }
        }

        // Инициализация списка провайдеров и создание словаря параметров для каждого из них (из ProvList.xml)
        void InitializeProviderList()
        {
            _provider_params = new Dictionary<string, Dictionary<string, string>>();
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load("ProvList.xml");
            for (int i = 0; i < xdoc.ChildNodes[1].ChildNodes.Count; i++)
            {
                var current_element = xdoc.ChildNodes[1].ChildNodes[i];
                _provider_params.Add(current_element.Attributes["name"].Value, InitializeParamList(i));
                cb_prov_name.Items.Add(current_element.Attributes["name"].Value);
            }
        }

        // Инициализация словаря параметров для каждого из провайдеров (из ProvList.xml)
        Dictionary<string, string> InitializeParamList(int index)
        {
            Dictionary<string,string> param = new Dictionary<string, string>();
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load("ProvList.xml");
            foreach (XmlElement x in xdoc.ChildNodes[1].ChildNodes[index])
                param.Add(x.Attributes["name"].Value, x.Attributes["value"].Value);
            return param;
        }

        // Заполнение поля формы элементами управления
        void FillParamContainer(object sender, EventArgs e)
        {
            flp_controls.Controls.Clear();
            Dictionary<string,string> dict=null;
            if (cb_prov_name.SelectedItem != null)
            {
                var val = _provider_params.TryGetValue(cb_prov_name.Text, out dict);
                if (dict != null)
                {
                    foreach (string key in dict.Keys)
                        flp_controls.Controls.Add(new ControlPair(key));
                }
            }
        }

        // Получение экземпляра Connection для выбранного провайдера
        IDbConnection GetCurrentProviderConnection()
        {
            return DbConnectionCollection.CreateConnection(cb_prov_name.Text);
        }

        // Обработка смены способа ввода строки подключения (radiobuttons)
        void HandleInputTypeChanges(object sender, EventArgs e)
        {
            if (rb_cs_manual.Checked)
            {
                Properties.Settings.Default.cs_manual_input = true;
                Properties.Settings.Default.cs_parametrized_input = false;
                txt_connstr.Enabled = true;
                flp_controls.Enabled = false;
            }
            else
            {
                Properties.Settings.Default.cs_manual_input = false;
                Properties.Settings.Default.cs_parametrized_input = true;
                txt_connstr.Enabled = false;
                flp_controls.Enabled = true;
            }
        }

        // Добавление обработчиков событий к контролам
        void ApplyHandlers()
        {
            cb_prov_name.SelectedValueChanged += this.FillParamContainer;
            cb_prov_name.SelectedValueChanged += this.BuildConnectionString;
            rb_cs_manual.CheckedChanged += this.HandleInputTypeChanges;
            rb_cs_parametrized.CheckedChanged += this.HandleInputTypeChanges;
        }

        // Формирование строки подключения
        void BuildConnectionString(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.cs_parametrized_input)
            {
                txt_connstr.Clear();
                Dictionary<string, string> dict = null;
                string value = null;
                _provider_params.TryGetValue(cb_prov_name.Text, out dict);
                if (dict == null)
                    return;
                else
                {
                    foreach (ControlPair cp in flp_controls.Controls)
                    {
                        if (cp.TextContent != "")
                        {
                            if (dict.Keys.Contains(cp.Description))
                                dict.TryGetValue(cp.Description, out value);
                            if (value != null)
                                txt_connstr.Text += string.Format("{0}{1};", value, cp.TextContent);
                        }
                    }
                }
            }
        }

        // Проверка подключения к БД
        private void btn_connect_Click(object sender, EventArgs e)
        {
            if (cb_prov_name.SelectedItem == null)
                return;
            try
            {
                BuildConnectionString(this, null);
                using (Checker ch = new Checker(GetCurrentProviderConnection()))
                {
                    ch.ConnectionString = txt_connstr.Text;
                    ch.CheckConnection();
                    MessageBox.Show("OK", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            //catch (MySqlException ex)
            //{
            //    string msg = AutoLocalizer.LocalizeExceptions(ex, this.LocalizeFile);
            //    MessageBox.Show(string.Format("Message: {0}", msg), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //catch (SqlException ex)
            //{
            //    string msg = AutoLocalizer.LocalizeExceptions(ex, this.LocalizeFile);
            //    MessageBox.Show(string.Format("Message: {0}", msg), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //catch (OracleException ex)
            //{
            //    string msg = AutoLocalizer.LocalizeExceptions(ex, this.LocalizeFile);
            //    MessageBox.Show(string.Format("Message: {0}", msg), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //catch (Exception ex)
            //{
            //    string msg = AutoLocalizer.LocalizeExceptions(ex, this.LocalizeFile);
            //    MessageBox.Show(string.Format("Message: {0}", msg), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            catch(DbExceptionCollection ex)
            {

            }
        }

        // Смена локализации из другого файла (xml)
        private void btn_lang_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "XML files|*.xml";
            var result = ofd.ShowDialog();
            if (result != DialogResult.OK)
                return;
            else
            {
                XmlDocument xdoc = new XmlDocument();
                try
                {
                    xdoc.Load(ofd.OpenFile());
                    if (xdoc != null)
                    {
                        AutoLocalizer.ApplyLocalization(this, ofd.SafeFileName);
                        Properties.Settings.Default.loc_filename_current = ofd.SafeFileName;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Message: {0}", ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
