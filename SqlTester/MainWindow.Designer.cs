﻿namespace SqlTester
{
    partial class MainWindow
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.btn_connect = new System.Windows.Forms.Button();
            this.cb_prov_name = new System.Windows.Forms.ComboBox();
            this.txt_connstr = new System.Windows.Forms.TextBox();
            this.desc_cs = new System.Windows.Forms.Label();
            this.flp_controls = new System.Windows.Forms.FlowLayoutPanel();
            this.desc_provider = new System.Windows.Forms.Label();
            this.rb_cs_manual = new System.Windows.Forms.RadioButton();
            this.rb_cs_parametrized = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_lang = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_connect
            // 
            this.btn_connect.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btn_connect.Location = new System.Drawing.Point(12, 200);
            this.btn_connect.Name = "btn_connect";
            this.btn_connect.Size = new System.Drawing.Size(187, 33);
            this.btn_connect.TabIndex = 0;
            this.btn_connect.Tag = "loc_btn_connect";
            this.btn_connect.Text = "Connect";
            this.btn_connect.UseVisualStyleBackColor = true;
            this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
            // 
            // cb_prov_name
            // 
            this.cb_prov_name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_prov_name.FormattingEnabled = true;
            this.cb_prov_name.Location = new System.Drawing.Point(12, 30);
            this.cb_prov_name.Name = "cb_prov_name";
            this.cb_prov_name.Size = new System.Drawing.Size(164, 21);
            this.cb_prov_name.TabIndex = 2;
            this.cb_prov_name.Tag = "";
            // 
            // txt_connstr
            // 
            this.txt_connstr.Enabled = false;
            this.txt_connstr.Location = new System.Drawing.Point(12, 174);
            this.txt_connstr.Name = "txt_connstr";
            this.txt_connstr.Size = new System.Drawing.Size(187, 20);
            this.txt_connstr.TabIndex = 3;
            this.txt_connstr.Tag = "";
            // 
            // desc_cs
            // 
            this.desc_cs.AutoSize = true;
            this.desc_cs.Location = new System.Drawing.Point(9, 158);
            this.desc_cs.Name = "desc_cs";
            this.desc_cs.Size = new System.Drawing.Size(89, 13);
            this.desc_cs.TabIndex = 11;
            this.desc_cs.Tag = "loc_desc_cs";
            this.desc_cs.Text = "Connection string";
            // 
            // flp_controls
            // 
            this.flp_controls.AutoScroll = true;
            this.flp_controls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flp_controls.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flp_controls.Location = new System.Drawing.Point(216, 12);
            this.flp_controls.Margin = new System.Windows.Forms.Padding(3, 3, 10, 10);
            this.flp_controls.Name = "flp_controls";
            this.flp_controls.Size = new System.Drawing.Size(221, 221);
            this.flp_controls.TabIndex = 12;
            this.flp_controls.WrapContents = false;
            // 
            // desc_provider
            // 
            this.desc_provider.AutoSize = true;
            this.desc_provider.Location = new System.Drawing.Point(9, 14);
            this.desc_provider.Name = "desc_provider";
            this.desc_provider.Size = new System.Drawing.Size(46, 13);
            this.desc_provider.TabIndex = 13;
            this.desc_provider.Tag = "loc_desc_provider";
            this.desc_provider.Text = "Provider";
            // 
            // rb_cs_manual
            // 
            this.rb_cs_manual.AutoSize = true;
            this.rb_cs_manual.Location = new System.Drawing.Point(9, 46);
            this.rb_cs_manual.Name = "rb_cs_manual";
            this.rb_cs_manual.Size = new System.Drawing.Size(167, 17);
            this.rb_cs_manual.TabIndex = 14;
            this.rb_cs_manual.Tag = "loc_rb_cs_manual";
            this.rb_cs_manual.Text = "Use my own connection string";
            this.rb_cs_manual.UseVisualStyleBackColor = true;
            // 
            // rb_cs_parametrized
            // 
            this.rb_cs_parametrized.AutoSize = true;
            this.rb_cs_parametrized.Checked = true;
            this.rb_cs_parametrized.Location = new System.Drawing.Point(9, 23);
            this.rb_cs_parametrized.Name = "rb_cs_parametrized";
            this.rb_cs_parametrized.Size = new System.Drawing.Size(136, 17);
            this.rb_cs_parametrized.TabIndex = 15;
            this.rb_cs_parametrized.TabStop = true;
            this.rb_cs_parametrized.Tag = "loc_rb_cs_params";
            this.rb_cs_parametrized.Text = "Use custom parameters";
            this.rb_cs_parametrized.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_cs_manual);
            this.groupBox1.Controls.Add(this.rb_cs_parametrized);
            this.groupBox1.Location = new System.Drawing.Point(12, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(185, 77);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Tag = "loc_groupname";
            this.groupBox1.Text = "Connection string Setup";
            // 
            // btn_lang
            // 
            this.btn_lang.BackgroundImage = global::SqlTester.Properties.Resources.lang1;
            this.btn_lang.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_lang.Location = new System.Drawing.Point(421, 243);
            this.btn_lang.Margin = new System.Windows.Forms.Padding(3, 3, 7, 7);
            this.btn_lang.Name = "btn_lang";
            this.btn_lang.Size = new System.Drawing.Size(40, 40);
            this.btn_lang.TabIndex = 17;
            this.btn_lang.UseVisualStyleBackColor = true;
            this.btn_lang.Click += new System.EventHandler(this.btn_lang_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(468, 288);
            this.Controls.Add(this.btn_lang);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.desc_provider);
            this.Controls.Add(this.flp_controls);
            this.Controls.Add(this.desc_cs);
            this.Controls.Add(this.txt_connstr);
            this.Controls.Add(this.cb_prov_name);
            this.Controls.Add(this.btn_connect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "loc_window_header";
            this.Text = "Connection test";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_connect;
        private System.Windows.Forms.ComboBox cb_prov_name;
        private System.Windows.Forms.TextBox txt_connstr;
        private System.Windows.Forms.Label desc_cs;
        private System.Windows.Forms.FlowLayoutPanel flp_controls;
        private System.Windows.Forms.Label desc_provider;
        private System.Windows.Forms.RadioButton rb_cs_manual;
        private System.Windows.Forms.RadioButton rb_cs_parametrized;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_lang;
    }
}

