﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using MySql.Data;
using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;

namespace SqlTester
{
    /// <summary>
    /// Класс для локализации приложения (на основе xml-содержимого)
    /// </summary>
    static class AutoLocalizer
    {
        // Применение локализации из выбранного файла
        public static void ApplyLocalization(Control in_ctrl, string in_fname)
        {
            var xdoc = new XmlDocument();
            xdoc.Load(in_fname);
            var dict = ExtractingStrings(xdoc);
            ReplacingString(dict, in_ctrl);
        }

        // Извлечение словаря из xml-файла для дальнейшей замены
        static Dictionary<string, string> ExtractingStrings(XmlDocument xd)
        {
            // выбираеся второй узел, т.к. в первом объявлены спецификации (кодировка и версия)
            var nodes = xd.ChildNodes[1].ChildNodes[0].ChildNodes;

            var dict = new Dictionary<string, string>(nodes.Count);
            foreach (XmlElement xe in nodes)
            {
                dict.Add(xe.Attributes["tag"].Value, xe.Attributes["content"].Value);
            }
            return dict;
        }

        // Подмена текста в контролах на содержимое словаря
        static void ReplacingString(Dictionary<string, string> in_dict, Control in_ctrl)
        {
            if (in_ctrl.Tag != null)
            {
                string filler = null;
                in_dict.TryGetValue(in_ctrl.Tag.ToString(), out filler);
                in_ctrl.Text = filler;
            }

            foreach (Control ctrl in in_ctrl.Controls)
            {
                if (ctrl.Controls.Count > 0)
                    ReplacingString(in_dict, ctrl);
                if (ctrl.Tag != null)
                {
                    string filler = null;
                    in_dict.TryGetValue(ctrl.Tag.ToString(), out filler);
                    ctrl.Text = filler;
                }
            }
        }

        // Возврат локализованного сообщения об ошибке (общее исключение)
        public static string LocalizeExceptions(Exception ex, string in_fname)
        {
            bool found = false;
            string result = null;
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(in_fname);
            if (xdoc == null)
                return null;
            foreach (XmlElement xe in xdoc.ChildNodes[1].ChildNodes[1].ChildNodes)
            {
                if (ex.GetType().Name == xe.Name)
                {
                    foreach (XmlElement x in xe.ChildNodes)
                    {
                        if (x.Attributes["code"].Value == ex.HResult.ToString())
                        {
                            result = x.Attributes["content"].Value;
                            found = true;
                        }
                    }
                }
            }
            if (!found)
                result = "Unknown error";
            return result;
        }

        // Возврат локализованного сообщения об ошибке (MySQL-исключение)
        public static string LocalizeExceptions(MySqlException ex, string in_fname)
        {
            bool found = false;
            string result = null;
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(in_fname);
            if (xdoc == null)
                return null;
            foreach (XmlElement xe in xdoc.ChildNodes[1].ChildNodes[1].ChildNodes)
            {
                if (ex.GetType().Name == xe.Name)
                {
                    foreach (XmlElement x in xe.ChildNodes)
                    {
                        if (x.Attributes["code"].Value == ex.Number.ToString())
                        {
                            result = x.Attributes["content"].Value;
                            found = true;
                        }
                    }
                }
            }
            if (!found)
                result = "Unknown error";
            return result;
        }

        // Возврат локализованного сообщения об ошибке (MSSQL-исключение)
        public static string LocalizeExceptions(SqlException ex, string in_fname)
        {
            bool found = false;
            string result = null;
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(in_fname);
            if (xdoc == null)
                return null;
            foreach (XmlElement xe in xdoc.ChildNodes[1].ChildNodes[1].ChildNodes)
            {
                if (ex.GetType().Name == xe.Name)
                {
                    foreach (XmlElement x in xe.ChildNodes)
                    {
                        if (x.Attributes["code"].Value == ex.Number.ToString())
                        {
                            result = x.Attributes["content"].Value;
                            found = true;
                        }
                    }
                }
            }
            if (!found)
                result = "Unknown error";
            return result;
        }

        public static string LocalizeExceptions(OracleException ex, string in_fname)
        {
            bool found = false;
            string result = null;
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(in_fname);
            if (xdoc == null)
                return null;
            foreach (XmlElement xe in xdoc.ChildNodes[1].ChildNodes[1].ChildNodes)
            {
                if (ex.GetType().Name == xe.Name)
                {
                    foreach (XmlElement x in xe.ChildNodes)
                    {
                        if (x.Attributes["code"].Value == ex.Number.ToString())
                        {
                            result = x.Attributes["content"].Value;
                            found = true;
                        }
                    }
                }
            }
            if (!found)
                result = "Unknown error";
            return result;
        }
    }
}
