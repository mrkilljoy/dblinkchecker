﻿using System;
using System.Data;

namespace SqlTester
{
    /// <summary>
    /// Класс, тестирующий подключение к БД
    /// </summary>
    public class Checker : IDisposable
    {
        bool _disposed;
        IDbConnection _conn;

        public Checker(IDbConnection conn)
        {
            _conn = conn;
        }
        
        ~Checker()
        {
            Dispose(false);
        }        

        #region Методы интерфейсов

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_conn != null)
                    {
                        _conn.Close();
                        _conn.Dispose();
                        _conn = null;
                    }
                    //if (_comm != null)
                    //{
                    //    _comm.Dispose();
                    //    _comm = null;
                    //}
                    //if (_ad != null)
                    //    _ad = null;
                }
                _disposed = true;
            }
        }

        #endregion

        // Подключение к БД
        public void CheckConnection()
        {
            if (_conn == null)
                throw new DbExceptionCollection();//Exception();
            else
            {
                try
                {
                    _conn.Open();
                    _conn.Close();
                    System.Threading.Thread.Sleep(500);
                }
                catch (DbExceptionCollection ex)
                {
                    throw ex;
                }
            }
        }

        public string ConnectionString
        {
            get
            {
                if (_conn == null)
                    return null;
                else
                    return _conn.ConnectionString;
            }
            set
            {
                if (_conn != null)
                {
                    _conn.ConnectionString = value;
                }
            }
        }
    }
}
